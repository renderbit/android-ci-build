FROM openjdk:8-jdk

LABEL maintainer="soham@renderbit.com"

ENV ANDROID_COMPILE_SDK="28"
ENV ANDROID_BUILD_TOOLS="28.0.2"
ENV ANDROID_SDK_TOOLS="4333796"

RUN apt-get update --yes && \
    apt-get upgrade --yes && \
    apt-get dist-upgrade --yes && \
    apt-get install --yes wget tar unzip lib32stdc++6 lib32z1 && \
    wget --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip && \
    unzip -d android-sdk-linux android-sdk.zip && \
    rm android-sdk.zip && \
    echo y | android-sdk-linux/tools/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" && \
    echo y | android-sdk-linux/tools/bin/sdkmanager "platform-tools" && \
    echo y | android-sdk-linux/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}"

# Temporarily disable checking for EPIPE error and use yes to accept all licenses
RUN bash -c "set +o pipefail && \
    yes | android-sdk-linux/tools/bin/sdkmanager --licenses && \
    set -o pipefail"

ENV ANDROID_HOME="${PWD}/android-sdk-linux"
ENV PATH="${PATH}:${PWD}/android-sdk-linux/platform-tools/"
